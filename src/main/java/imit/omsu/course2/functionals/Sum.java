package imit.omsu.course2.functionals;


import imit.omsu.course2.interfaces.Function;
import imit.omsu.course2.interfaces.Functional;

public class Sum implements Functional {

    public double calculate(Function f){
        return f.calculation(f.getLowerLimit())+
                f.calculation(f.getUpperLimit())+
                f.calculation((f.getUpperLimit()+f.getLowerLimit())/2);
    }
}