package imit.omsu.course2.interfaces;

public interface Function {
    public double calculation(double x);
    public double getLowerLimit();
    public double getUpperLimit();
}
