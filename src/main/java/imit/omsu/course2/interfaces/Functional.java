package imit.omsu.course2.interfaces;

public interface Functional <T extends Function> {
    public double calculate(T f);
}
